﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;

namespace Semillas.Website.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PingController : ControllerBase
    {
        private readonly IWebHostEnvironment _environment;

        public PingController(
            IWebHostEnvironment environment)
        {
            _environment = environment;
        }

        [HttpGet]
        public IActionResult Ping()
        {
            var environmentName = _environment.EnvironmentName;
            var currentDateTime = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture);
            var assemblyName = typeof(Startup).Assembly.GetName();
            var result = $"Assembly={assemblyName}, Environment={environmentName}, CurrentTime={currentDateTime}";
            return Ok(result);
        }
    }
}
