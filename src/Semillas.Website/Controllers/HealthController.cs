﻿using Microsoft.AspNetCore.Mvc;

namespace Semillas.Website.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public IActionResult HealthCheck()
        {
            return Ok("running");
        }
    }
}
