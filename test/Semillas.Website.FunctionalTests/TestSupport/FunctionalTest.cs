﻿using System;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Semillas.TestSupport.XUnit;
using Semillas.Website.FunctionalTests.TestSupport.Extensions;

namespace Semillas.Website.FunctionalTests.TestSupport
{
    public abstract class FunctionalTest
        : Given_When_Then_Test_Async
    {
        private readonly IServiceProvider _serviceProvider;
        protected HttpClient HttpClient { get; }
        protected IConfiguration Configuration { get; }
        protected FunctionalTest()
        {
            var server =
                new TestServer(
                    new WebHostBuilder()
                        .UseStartup<Startup>()
                        .UseCommonConfiguration()
                        .UseEnvironment("Test")
                        .ConfigureTestServices(ConfigureTestServices));

            HttpClient = server.CreateClient();
            _serviceProvider = server.Services;
            Configuration = _serviceProvider.GetService<IConfiguration>();
        }

        protected T GetService<T>() where T : class
        {
            return _serviceProvider.GetService<T>();
        }

        protected virtual void ConfigureTestServices(IServiceCollection services)
        {
        }
    }
}