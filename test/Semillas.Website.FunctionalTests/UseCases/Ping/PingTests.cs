﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Semillas.Website.FunctionalTests.TestSupport;
using Semillas.Website.FunctionalTests.TestSupport.Builders;
using Xunit;

namespace Semillas.Website.FunctionalTests.UseCases.Ping
{
    public static class GetConfigurationValueTests
    {
        public class Given_A_Ping_Url_When_Pinging
            : FunctionalTest
        {
            private HttpResponseMessage _result;
            private string _url;

            protected override Task Given()
            {
                _url = "ping";
                
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
    }
}