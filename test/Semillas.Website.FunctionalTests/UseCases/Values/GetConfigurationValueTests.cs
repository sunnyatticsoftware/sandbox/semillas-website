﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Semillas.Website.FunctionalTests.TestSupport;
using Xunit;

namespace Semillas.Website.FunctionalTests.UseCases.Values
{
    public static class GetConfigurationValueTests
    {
        public class Given_A_Foo_Configuration_Id_When_Getting_Value
            : FunctionalTest
        {
            private HttpResponseMessage _result;
            private string _url;

            protected override Task Given()
            {
                _url = "values/Foo";
                
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }

            [Fact]
            public async Task Then_It_Should_Return_The_Expected_Value_From_Test_Settings()
            {
                var content = await _result.Content.ReadAsStringAsync();
                content.Should().Be("TestValue");
            }
        }
    }
}